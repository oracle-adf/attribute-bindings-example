package org.javadev.view;

import javax.faces.event.ActionEvent;

public class MyBean {
    
    
    public MyBean() {
        super();
    }

    public void onButtonClick(ActionEvent actionEvent) {
        boolean result = (Boolean)ADFUtils.getBoundAttributeValue("MySwitchAttr");
        ADFUtils.setBoundAttributeValue("MySwitchAttr", result ? false : true);
    }
}
